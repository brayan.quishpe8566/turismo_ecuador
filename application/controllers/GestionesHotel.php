<?php
include("application/libraries/GroceryCRUDEnterprise/autoload.php");
use GroceryCrud\Core\GroceryCrud;
class GestionesHotel extends CI_Controller{

    //Constructor de la clase (Ojo doble guion bajo __)
    public function __construct(){
      parent::__construct();
      //cargar la libreria en este controlador
      $this->load->database();
      $this->load->library('grocery_CRUD');

      //Verificar si existe o no alguien conectado
      // if(!$this->session->userdata("usuarioC0nectado")){
      //     $this->session->set_flashdata("error","Por favor Inicie Sesion");
      //     redirect('seguridades/cerrarSesion');
      // }else{//Codigo cuando SI esta conectado
      //   if(!($this->session->userdata("usuarioC0nectado")["perfil"]=="ADMINISTRADOR"
      //     || $this->session->userdata("usuarioC0nectado")["perfil"]=="VENDEDOR")){
      //       redirect('seguridades/cerrarSesion');
      //   }
      // }
    }
    public function index(){
      $this->tmplateOutput((object)array('output'=>'','js_files'=>[], 'css_files'=>[]));
    }
    public function gestionHoteles(){
      $clientes=new grocery_CRUD();
      // $clientes->set_relation('fk_id_cli','cliente','nombre_cli');
      // $clientes->field_type('destino_res','dropdown',array('1' => 'Quito', '2' => 'Cuenca', '3' => 'Guayaquil', '4' => 'Ambato', '5' => 'Manabi', '6' => 'Esmeraldas',));
      $clientes->set_table('hotel');
      $clientes->set_language('spanish');
      $clientes->set_theme('flexigrid');
      $this->templateOutput($clientes->render());
      // $clientes->set_field_upload('imagen_portada_peli','uploads/peliculas');

      // con sta linea podemos cambiar el nombre de los campos que vienen de la db
      $clientes->display_as('id_hot','ID');
      $clientes->display_as('nombre_hot','Hotel');
      $clientes->display_as('direccion_hot','Direccion');
      $clientes->display_as('precio_hot','Precio');
      // $clientes->set_subject('Cliente');
      // $output=$clientes->render();
      // $this->load->view('encabezado');
      // $this->load->view('gestionesHotel/gestionHoteles',$output);
      // $this->load->view('pie');
    }
    public function templateOutput($output = null)
    {
      if ($output->isJSONResponse)
      {
        header('Content-Type: application/json; charset=utf-8');
        echo $output->output;
        exit;
      }
    }
  }
 ?>
